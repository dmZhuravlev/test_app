/**
* init file
*/
var express  = require('express');
var app = express();
var logger = require('morgan');
var path = require('path');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var methodOverride = require('method-override');

var port = process.env.PORT || 8080;

var dbPath = 'mongodb://zhur:access@ds053788.mongolab.com:53788/storage';
mongoose.connect(dbPath, function (err) {
    if (err) throw err;
    console.log('Successfully connected to ' + dbPath);
});

app.use(logger('dev'));         // log every request to the console
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(expressValidator());
app.use(bodyParser.json());   // pull information from html in POST
app.use(methodOverride());    // simulate DELETE and PUT


// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');

// ROUTES
require('./app/routes/routes.js')(app);

//REST API
var methods = require('./app/controllers/user_methods.js');
app.post('/adding', function(req, res){
    methods.addUser(req, res)
});

app.listen(port);
console.log('The server started on port ' + port);

//// Yes! You can use the model defined in the models/user.js directly
//var UserModel = mongoose.model('User');
//
//// Or, you can use it this way:
//UserModel = models.User;
//
//app.get('/', function(req, res) {
//  var user = new UserModel();
//  user.name = 'bob';
//  user.save();
//  // UserModel.getUserByUsername();
//});
//http://webapplog.com/express-js-and-mongoose-example-building-hackhall/
//https://devcenter.heroku.com/articles/nodejs-mongoose
//http://scotch.io/tutorials/javascript/easy-node-authentication-setup-and-local