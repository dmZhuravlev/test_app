var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
    first_name: {
      type: String
    },
    last_name: {
        type: String
    },
    email: {
        type:String
    },
    day: {
        type: Number
    },
    month: {
        type:String
    },
    year: {
        type: Number
    },
    footSize: {
        type: Number
    }
}, { collection: 'userInfo' });
var User = mongoose.model('userInfo', userSchema);
module.exports = User;

//var User = module.exports = mongoose.model('User', userSchema);
//module.exports.getUserById = function(id, callback) {
//  User.findById(id, callback);
//}
//
//module.exports.getUserByUsername = function(username, callback) {
//  var query = {username: username};
//  User.findOne(query, callback);
//}