/*
 *
 * basic method
 */
var user = require('../models/user.js');
//not used anymore
//var util = require('util');

module.exports = {
    addUser: function (req, res) {
        var errors;

        req.assert('first_name', 'first name is required').notEmpty();
        req.assert('last_name', 'last name is required').notEmpty();
        req.assert('email', 'set correct email').isEmail();
        req.assert('day', 'day is required').notEmpty();
        req.assert('month', 'month is required').notEmpty();
        req.assert('year', 'year is required').notEmpty();
        req.assert('footSize', 'set correct size').notEmpty().isInt();

        errors = req.validationErrors();
        if (errors) {
            res.status('400').send(errors);
            return;
        }
        new user({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            day: req.body.day,
            month: req.body.month,
            year: req.body.year,
            footSize: req.body.footSize
        }).save(function(err){
            if (err) throw err;
            //res.redirect('/step2');
            res.send({'redirect': '/step2'});
        });
    }
};
