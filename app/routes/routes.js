// app
module.exports = function(app) {
    // HOME PAGE 
    app.get('/', function(req, res) {
        res.redirect('/step1');
    });

    // STEP1
    app.get('/step1', function(req, res) {
        res.render('step1.ejs');
    });

    // STEP2
    app.get('/step2', function(req, res) {
        res.render('step2.ejs');
    });

    // STEP3
    app.get('/step3', function(req, res) {
        res.render('step3.ejs');
    });
};