/**
 *
 * client validation
 */
(function($) {
    $(document).ready(function(){
        $('#form-registration').on('submit', function (e) {
            e.preventDefault();
            var hasEmpty = false;
            var requestObj = {};
            $(this).find('input').each(function () {
                var that = $(this);
                var parent = that.closest('.wrap-field');
                if (!$.trim(that.val())) {
                    parent.find('.error-massage').addClass('show');
                    hasEmpty = true;
                } else {
                    parent.find('.error-massage').removeClass('show');
                }
            });

            $(this).find('select').each(function () {
                var that = $(this);
                var parent = that.closest('.wrap-field');
                if (that.val() === '-1') {
                    parent.find('.error-massage').addClass('show');
                    hasEmpty = true;
                    return false;
                } else {
                    parent.find('.error-massage').removeClass('show');
                }
            });
            if ( hasEmpty ) {
                return false;
            } else {
                var formData = {
                    'first_name': $('#first-name').val(),
                    'last_name': $('#last-name').val(),
                    'email': $('#input-email').val(),
                    'day': $('#birthday_day').val(),
                    'month': $('#birthday_month').val(),
                    'year': $('#birthday_year').val(),
                    'footSize': $('#shoe-size').val()
                };

                $.ajax({
                    type: 'POST',
                    url: '/adding',
                    data: formData,
                    dataType: 'json',
                    encode: true,
                    success: function(data) {
                        console.log('data', data.redirect);
                        //window.location.assign = data.redirect;
                        window.location = data.redirect;
                    },
                    error: function(data) {
                        var errorArray = [];
                        var showError = function (selector, msg) {
                            $(selector).closest('.wrap-field').find('.error-massage').text(msg).addClass('show');
                        };
                        var objMatches = {
                            'first_name': '#first-name',
                            'last_name': '#last-name',
                            'email': '#input-email',
                            'day': '#birthday_day',
                            'month': '#birthday_month',
                            'year', '#birthday_year',
                            'footSize', '#shoe-size'
                        }
                        if (data.responseText) {
                            errorArray = JSON.parse(data.responseText);
                        }

                        for(var key = 0; key <= errorArray.length - 1; key++) {
                            var param = errorArray[key]['param'];

                            showError(objMatches[param], errorArray[key]['msg']);
                            
                        }
                    }
                });
            }
        });
    });
}(window.jQuery));